import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React from 'react';
import Ionicons from 'react-native-vector-icons/dist/Ionicons';
import {ScrollView} from 'react-native-gesture-handler';
import {Color} from '../../../component/global/GlobalStyles';
import {Colors} from '../../../styles';
import {TextBold, TextRegular} from '../../global/Text';

const BerandaComponent = ({navigation, category, brand, lastSeen}) => {
  console.log(lastSeen);
  const RenderItemCategory = ({item}) => {
    return (
      <View
        style={{
          width: 34,
          alignItems: 'center',
          justifyContent: 'center',
          marginRight: 16,
        }}>
        <View
          style={{
            width: 34,
            height: 34,
            borderRadius: 34,
            backgroundColor: '#FFAB6C',
            marginBottom: 8,
          }}></View>
        <TextRegular text={item.name} size={10} />
      </View>
    );
  };
  const RenderItemBrand = ({item}) => {
    return (
      <View style={{borderWidth: 2, borderColor: Colors.GREY}}>
        <Image
          source={{uri: item.image_url}}
          style={{aspectRatio: 1, resizeMode: 'contain', width: 80}}
        />
      </View>
    );
  };
  const RenderItemLastSeen = ({item}) => {
    console.log('ini item', item);
    return (
      <View
        style={{
          padding: 6,
          backgroundColor: Colors.WHITE,
          marginLeft: 16,
          alignItems: 'center',
          alignContent: 'center',
          borderRadius: 6,
        }}>
        <View>
          {/* <Image
            source={{uri: item.images[0].image_url}}
            style={{
              aspectRatio: 1,
              resizeMode: 'contain',
              width: 120,
              alignSelf: 'center',
            }}
          /> */}
          <TextRegular
            text={item.name}
            size={12}
            style={{width: 152, marginTop: 14}}
            color={Colors.GREY}
          />
          <View style={{flexDirection: 'row'}}>
            <TextBold
              text={'Rp '}
              size={14}
              color={Colors.BLACK}
              style={{alignSelf: 'flex-start'}}
            />
            <TextBold
              text={item.price}
              size={14}
              color={Colors.BLACK}
              style={{alignSelf: 'flex-start'}}
            />
          </View>
        </View>
      </View>
    );
  };

  return (
    <View>
      <View style={styles.viewHeader}>
        <View style={styles.search}>
          <TextInput placeholder="Cari Produk" />
        </View>
        <TouchableOpacity>
          <Ionicons name="chatbox-ellipses" size={24} color="#D1D5DC" />
        </TouchableOpacity>
        <TouchableOpacity>
          <Ionicons name="notifications" size={24} color="#D1D5DC" />
        </TouchableOpacity>
      </View>
      <ScrollView>
        <View
          style={{
            backgroundColor: Colors.BLUETOYOTADETAIL,
            width: '100%',
            height: 134,
          }}></View>

        <ScrollView
          horizontal={true}
          style={{
            flexDirection: 'row',
            paddingVertical: 16,
            paddingHorizontal: 16,
            width: '100%',
          }}>
          <View
            style={{
              flexDirection: 'row',
              alignContent: 'flex-end',
            }}>
            <FlatList
              initialNumToRender={4}
              horizontal={true}
              data={category.data}
              renderItem={({item}) => <RenderItemCategory item={item} />}
            />
          </View>
        </ScrollView>
        <View style={{width: '100%'}}>
          <Image
            source={require('../../../asset/images/expressorder.png')}
            style={{
              aspectRatio: 4,
              resizeMode: 'contain',
              width: '100%',
            }}
          />
        </View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            padding: 16,
            width: '100%',
            alignItems: 'center',
            alignContent: 'center',
          }}>
          <TextBold text={'Brand Pilihan'} color={Colors.BLACK} size={16} />
          <TextRegular text={'Lihat Semua'} color={Colors.GREY} size={14} />
        </View>
        <FlatList
          initialNumToRender={4}
          horizontal={true}
          data={brand.data}
          renderItem={({item}) => <RenderItemBrand item={item} />}
        />
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            padding: 16,
            width: '100%',
            alignItems: 'center',
            alignContent: 'center',
          }}>
          <TextBold text={'Terakhir Dilihat'} color={Colors.BLACK} size={16} />
          <TextRegular text={'Lihat Semua'} color={Colors.GREY} size={14} />
        </View>
        <FlatList
          initialNumToRender={4}
          horizontal={true}
          data={lastSeen.data}
          renderItem={({item}) => <RenderItemLastSeen item={item} />}
        />
        <View style={{marginBottom: 200}}></View>
      </ScrollView>
    </View>
  );
};

export default BerandaComponent;

const styles = StyleSheet.create({
  search: {
    borderRadius: 6,
    borderColor: '#D1D5DC',
    borderWidth: 1,
    width: '70%',
  },
  viewHeader: {
    flexDirection: 'row',
    marginHorizontal: 16,
    marginVertical: 8,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
