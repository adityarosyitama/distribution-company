import {
  Image,
  ImageBackground,
  KeyboardAvoidingView,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React from 'react';
import Fontisto from 'react-native-vector-icons/dist/Fontisto';
import MaterialCommunityIcons from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import LinearGradient from 'react-native-linear-gradient';
import {TextBold, TextRegular} from '../../../component/global/Text';
import {Colors} from '../../../styles';
import {InputText} from '../../../component/global/InputText';

const LoginComponent = ({navigation, onSave, noHp, setNoHp}) => {
  return (
    <View style={styles.container}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
          paddingBottom: 10,
          height: '100%',
          // justifyContent: 'space-between',
        }}>
        {/* <KeyboardAvoidingView
          behavior="padding"
          enabled
          keyboardVerticalOffset={-500}> */}
        <ImageBackground
          source={require('../../../asset/images/imagelogin.png')}
          style={{
            alignSelf: 'center',
            width: '100%',
            height: 320,
            justifyContent: 'flex-end',
          }}>
          <TextBold
            text="Masuk Ke Akun Anda"
            size={28}
            color={Colors.WHITE}
            style={{padding: 16}}
          />
        </ImageBackground>
        <View style={styles.input}>
          <View style={{flexDirection: 'row'}}>
            <TextRegular text={'Nomor Telepon'} />
            <TextRegular text={'*'} color={Colors.RED} />
          </View>
          <InputText
            placeholderText="08xxx"
            style={{paddingHorizontal: 8, marginTop: 6}}
            keyboardType="numeric"
            value={noHp}
            onChangeText={text => setNoHp(text)}
          />
        </View>
        <View
          style={{
            // justifyContent: 'flex-end',
            alignItems: 'center',
            marginBottom: 12,
            paddingHorizontal: 16,
            // backgroundColor: 'blue',
            // height: '100%',
            bottom: 0,
            position: 'absolute',
            width: '100%',
          }}>
          <TouchableOpacity
            onPress={() => {
              const params = {
                phone_number: noHp,
              };
              onSave(params);
            }}
            style={styles.bottom}>
            <TextBold text={'MASUK'} color={Colors.WHITE} />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigation.navigate('BottomNav')}
            style={{marginTop: 6}}>
            <TextBold text={'Lewati ke Beranda'} />
          </TouchableOpacity>
        </View>
        {/* </KeyboardAvoidingView> */}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    // backgroundColor: 'red',
    flex: 1,
  },
  welcome: {
    fontSize: 24,
    fontWeight: '700',
    color: 'black',
    alignSelf: 'center',
    marginTop: 22,
  },
  input: {
    margin: 16,
  },
  bottom: {
    borderRadius: 6,
    width: '100%',
    padding: 10,
    alignItems: 'center',
    // alignSelf: 'center',
    backgroundColor: 'blue',
  },
});

export default LoginComponent;
