import {StyleSheet, Text, View} from 'react-native';
import React, {useState} from 'react';
import LoginComponent from '../../components/section/Auth/LoginComponent';
import {Colors} from '../../styles';
import apiProvider from '../../utils/service/apiProvider';
import {useDispatch, useSelector} from 'react-redux';
import {useIsFocused} from '@react-navigation/native';
import {useEffect} from 'react';

const Login = ({navigation}) => {
  const [Nohp, setNohp] = useState('');
  const isFocused = useIsFocused();
  const [requestToken, setRequestToken] = useState('');
  const dispatch = useDispatch();
  const {loginData, isLoggedIn} = useSelector(state => state.login);
  const onSave = async params => {
    const response = await apiProvider.postDataLogin(params);
    console.log('ini respon', response);
    const token = await apiProvider.postDataToken(
      response.data.otp_request_token,
    );
    console.log('ini token baru', token.data.data.api_token);
    const data = {
      requestToken: response.data.otp_request_token,
      id: params,
      token: token.data.data.api_token,
    };
    dispatch({type: 'ADD_DATA_LOGIN', data: data});
  };

  const onUpdateToken = async () => {
    const response = await apiProvider.postDataLogin(loginData.id);
    const token = await apiProvider.postDataToken(
      response.data.otp_request_token,
    );
    console.log('ini token baru', token);
    const data = {
      requestToken: response.data.otp_request_token,
      id: loginData.id,
      token: token.data.data.api_token,
    };
    dispatch({type: 'ADD_DATA_LOGIN', data: data});
  };
  useEffect(() => {
    if (isLoggedIn != false) {
      onUpdateToken();
      navigation.replace('BottomNav');
    } else {
    }
  }, [isFocused]);

  return (
    <View style={{flex: 1, backgroundColor: Colors.WHITE}}>
      <LoginComponent
        navigation={navigation}
        onSave={onSave}
        noHp={Nohp}
        setNoHp={setNohp}
      />
    </View>
  );
};
export default Login;
const styles = StyleSheet.create({});
