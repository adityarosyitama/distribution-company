import {Image, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {
  Color,
  FontFamily,
  FontSize,
  Padding,
  Border,
} from '../../component/global/GlobalStyles';
import HomeComponent from '../../component/section/home/HomeComponent';
import BerandaComponent from '../../components/section/Beranda/BerandaComponent';
import {useSelector} from 'react-redux';
import apiProvider from '../../utils/service/apiProvider';
import {useIsFocused} from '@react-navigation/native';
import {useEffect} from 'react';
import {useState} from 'react';

const Home = navigation => {
  const isFocused = useIsFocused();
  const [category, setCategory] = useState('');
  const [brand, setBrand] = useState('');
  const [lastSeen, setLastSeen] = useState('');
  const {loginData, isLoggedIn} = useSelector(state => state.login);
  const getCategory = async () => {
    const response = await apiProvider.getCategory(loginData.token);
    if (response) {
      setCategory(response);
    }
  };
  const getBrand = async () => {
    const response = await apiProvider.getBrand(loginData.token);
    console.log('ini get brand', response);
    if (response) {
      setBrand(response);
    }
  };
  const getLastSeen = async () => {
    const response = await apiProvider.getLastSeen(loginData.token);
    if (response) {
      setLastSeen(response);
    }
  };

  useEffect(() => {
    getCategory();
    getBrand();
    getLastSeen();
  }, [isFocused]);
  return (
    <BerandaComponent
      navigation={navigation}
      category={category}
      brand={brand}
      lastSeen={lastSeen}
    />
  );
};

export default Home;
