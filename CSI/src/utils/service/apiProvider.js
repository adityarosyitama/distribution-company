import axios from 'axios';
import {BASE_URL} from './url';

const API = async (
  url,
  options = {
    method: 'GET',
    body: {},
    head: {},
  },
) => {
  const request = {
    baseURL: BASE_URL,
    method: options.method,
    timeout: 10000,
    url,
    headers: options.head,
    responseType: 'json',
  };
  if (
    request.method === 'POST' ||
    request.method === 'PUT' ||
    request.method === 'DELETE' ||
    request.method === 'PATCH'
  )
    request.data = options.body;

  const res = await axios(request);

  if (res.status === 200) {
    return res.data;
  } else {
    return res;
  }
};

export default {
  postDataLogin: async params => {
    return API(`api/login/v2`, {
      method: 'POST',
      body: params,
      head: {
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        return response;
      })
      .catch(error => {
        return error;
      });
  },
  postDataToken: async params => {
    const data = {
      otp_request_token: params,
      otp_code: '91010',
    };
    console.log('ini data dari token', data);
    return API(`api/otp`, {
      method: 'POST',
      body: data,
      head: {
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        return response;
      })
      .catch(error => {
        return error;
      });
  },
  getCategory: async params => {
    return API(`api/categories/6/childs`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${params}`,
      },
    })
      .then(response => {
        return response;
      })
      .catch(error => {
        return error;
      });
  },
  getBrand: async params => {
    console.log(params);
    return API(`api/brands`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${params}`,
      },
    })
      .then(response => {
        return response;
      })
      .catch(error => {
        return error;
      });
  },
  getLastSeen: async params => {
    return API(`api/products/seen`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${params}`,
      },
    })
      .then(response => {
        return response;
      })
      .catch(error => {
        return error;
      });
  },
};
