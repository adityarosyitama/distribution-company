import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import BottomNav from './BottomNav';
import Login from '../screen/Auth/Login';
import Register from '../screen/Auth/Register';
import Splashscreen from '../screen/Auth/Splashscreen';

const Stack = createStackNavigator();

export default function AuthNavigation() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Register" component={Register} />
      <Stack.Screen name="BottomNav" component={BottomNav} />
    </Stack.Navigator>
  );
}
