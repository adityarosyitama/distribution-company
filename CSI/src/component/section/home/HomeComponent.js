import {Image, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {
  Color,
  FontFamily,
  FontSize,
  Padding,
  Border,
} from '../../component/global/GlobalStyles';
import {Colors} from '../../../styles';
import {TextInput} from 'react-native-gesture-handler';
const HomeComponent = navigation => {
  return (
    <View style={{flex: 1}}>
      <View
        style={{
          width: '100%',
          backgroundColor: Colors.BLUETOYOTADETAIL,
          flexDirection: 'row',
          alignItems: 'center',
          paddingVertical: 8,
          paddingHorizontal: 16,
        }}>
        <View
          style={{
            width: '70%',
            backgroundColor: Colors.WHITE,
            borderRadius: 6,
            marginRight: 28,
            paddingLeft: 16,
          }}>
          <View
            style={{
              width: '100%',
              backgroundColor: Colors.WHITE,
              borderRadius: 6,
              marginRight: 28,
              paddingLeft: 16,
            }}>
            <TextInput placeholder="Cari Produk" />
          </View>
        </View>
        <Image
          source={require('../../../assets/massage.png')}
          style={{width: 24, height: 24, marginRight: 28}}
        />
        <Image
          source={require('../../../assets/bell.png')}
          style={{width: 24, height: 24}}
        />
      </View>
    </View>
  );
};

export default HomeComponent;
